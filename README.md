# Building an Application with Spring Boot #

Tutorial ini membuat applikasi restfull simple dengan menggunakan Springboot.

### Cara Menggunakan Student REST API ###

https://localhost:8080/students?studentId=120

### Hasilnya ###

{
  "Student": {
    "studentId": "120",
    "name": "Ninja Panda",
    "rollNo": "CS-120",
    "department": "Computer Science",
    "shortCode": "CS",
    "year": "2nd"
  }
}